import App from './App'
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import uView from '@/uni_modules/uview-ui'
Vue.config.productionTip = false
App.mpType = 'app'
import toolsMethods from './utils/tools.js'
Vue.use(VueI18n);
Vue.use(uView)
Vue.prototype.$to = toolsMethods.jumpTo;
Vue.prototype.$tips = toolsMethods.showTips;
Vue.prototype.$tools = toolsMethods;
const i18n = new VueI18n({
	// #ifdef APP-PLUS
	locale: plus.storage.getItem('locale') || 'zh-CN', //初始化,保证刷新页面也保留
	// #endif
	// #ifdef H5
	locale: localStorage.getItem('locale') || 'zh-CN', //初始化,保证刷新页面也保留
	// #endif
	// 加载语言文件的内容
	messages: {
		'zh-CN': require('./locales/zh-CN.js').lang,
		'zh-TW': require('./locales/zh-TW.js').lang,
		'en': require('./locales/en.js').lang,
	}
})
const app = new Vue({
	i18n,
	...App
})
app.$mount()