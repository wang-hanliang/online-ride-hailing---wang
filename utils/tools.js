const toolsMethods = {
	//路由跳转
	jumpTo(url, query = {}, type) {
		if (!url || url.toString().indexOf('/') === -1) {
			console.log(Number(url) > 0 ? Number(url) : 1, 'back页码');
			let pages = getCurrentPages();
			if (pages.length === 1 && !url) {
				uni.reLaunch({
					url: '/pages/tabbar/home'
				})
			} else {
				uni.navigateBack({
					delta: Number(url) > 0 ? Number(url) : 1
				});
			}
		} else {
			let tabBar = ['/pages/tabbar/home', '/pages/tabbar/bookFriend', '/pages/tabbar/circle',
				'/pages/tabbar/mine',
			];
			let iftabBar = tabBar.filter(item => item === url);
			let jumpType = type || 0;
			let quertStr = '?';
			if (query) {
				let keys = Object.keys(query);
				let values = Object.values(query);
				keys.forEach((item, index) => {
					if (index > 0) {
						quertStr += '&' + item + '=' + values[index];
					} else {
						quertStr += item + '=' + values[index];
					}
				})
			}
			if (iftabBar && iftabBar.length > 0) {
				uni.switchTab({
					url: query ? iftabBar[0] + '' + quertStr : iftabBar[0],
				})
			} else {
				//关闭当前页面
				if (jumpType === 1) {
					uni.redirectTo({
						url: query ? url + '' + quertStr : url,
					})
				} else if (jumpType === 2) {
					//关闭所有页面
					uni.reLaunch({
						url: query ? url + '' + quertStr : url,
					})
				} else {
					uni.navigateTo({
						url: query ? url + '' + quertStr : url,
						animationType: "slide-in-right",
					})
				}
			}
		}
	},
	//获取当前语言
	getLocale() {
		// #ifdef APP-PLUS
		return plus.storage.getItem('locale') || 'zh-CN' //初始化,保证刷新页面也保留
		// #endif
		// #ifdef H5
		return localStorage.getItem('locale') || 'zh-CN' //初始化,保证刷新页面也保留
		// #endif
	},
	//弹窗提醒
	showTips(msg, Fn, time = 1550) {
		console.log(time);
		// #ifdef APP
		plus.nativeUI.toast(msg, {
			icon: '/static/icons/client/faceBookLogin.png',
			iconWidth: '18px',
			iconHeight: '18px',
			style: "inline",
			align: "center"
		});
		// #endif
		// #ifndef APP 
		uni.showToast({
			title: msg,
			icon: 'none',
			mask: true,
			duration: time
		});
		// #endif
		if (Object.prototype.toString.apply(Fn) === '[object Function]') {
			setTimeout(() => {
				Fn();
			}, time);
		}
	},
}

export default toolsMethods