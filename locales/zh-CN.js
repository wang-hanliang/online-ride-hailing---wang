export const lang = {
	"account": {
		"login": {
			"language": {
				"Chinese": '中文',
				"complexFont": '繁体',
				"English": '英文'
			},
			"loginType": {
				"accountLogin": '账号登录',
				"codeLogin": '验证码登录'
			},
			"form": {
				"accountPlaceHolder": '输入账号',
				"passwordPlaceHolder": '请输入密码(8位字母数字组合)',
				"phonePlaceHolder": '请输入手机号',
				"codePlaceHolder": '请输入验证码',
				"getCode": '获取验证码',
			},
			"btnText": '登录',
			"other": {
				"register": '注册',
				"forgetPassword": '忘记密码?',
			},
			"tripartite": {
				"FaceBook": 'FaceBook',
				"Google": 'Google',
				"appleLogin": '苹果ID登录',
				"wxLogin": '微信登录'
			},
			"agreement": {
				"rule1": '阅读并同意',
				"rule2": '《用户协议》',
				"rule3": '《隐私政策》'
			}
		},
		"register": {
			"title": '注册账号',
			"form": {
				"phonePlaceHolder": '请输入手机号',
				"codePlaceHolder": '请输入验证码',
				"getCode": '获取验证码',
				"passwordPlaceHolder": '请设置密码(8位字母数字组合)',
				"passwordPlaceHolderAgain": '请确认密码(8位字母数字组合)',
				"applyPlaceHolder": '请输入邀请码',
				"littleTip": '如没有邀请码直接略过'
			},
			"btnText": '注册'
		},
		"findPassword": {
			"btnText": '确定'
		},
		"faceRecognition": {
			"title": '人脸识别',
			"join": '加入',
			"confirm": '确认'
		}
	}
}